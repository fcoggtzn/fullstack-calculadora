package com.ovante.fggn.uaa.suma;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ovante.fggn.uaa.suma.servicio.CalculadoraInterface;

@SpringBootTest
class SumaEjemploApplicationTests {
	
	@Autowired
	CalculadoraInterface calculadora;

	@Test
	void contextLoads() {
		System.out.println("p1");
		// sumando x =5  y =6 --- espera 11
		double x= 5.0;
		double y= 6.0;
		double resultado;
	    resultado = calculadora.suma(x, y);
	    assertEquals(resultado, 11.0);
	}

	
	@Test
	void prueba2() {
		System.out.println("p2");
		// sumando x =5  y =6 --- espera 11
		double x= -5.0;
		double y= 6.0;
		double resultado;
	    resultado = calculadora.suma(x, y);
	    assertEquals(resultado, 1.0);
	}

}
