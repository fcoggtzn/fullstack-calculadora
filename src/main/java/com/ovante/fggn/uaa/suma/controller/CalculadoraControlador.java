package com.ovante.fggn.uaa.suma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ovante.fggn.uaa.suma.servicio.CalculadoraInterface;

@RestController
@RequestMapping(path="/cal") // This means URL's start with /demo (after Application path)
public class CalculadoraControlador {
   
	@Autowired
	CalculadoraInterface calculadora;
	
	 @CrossOrigin(origins = "*")
	 @GetMapping(path="/")
	public double suma(@RequestParam double x ,@RequestParam  double y ) {
		return calculadora.suma(x, y);
	}
}
