package com.ovante.fggn.uaa.suma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SumaEjemploApplication {

	public static void main(String[] args) {
		SpringApplication.run(SumaEjemploApplication.class, args);
	}

}
