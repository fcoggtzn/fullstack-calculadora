package com.ovante.fggn.uaa.suma.servicio;

public interface CalculadoraInterface {
	
	public double suma(double x, double y);

}
